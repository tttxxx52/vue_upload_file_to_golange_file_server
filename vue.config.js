const path = require('path')
const webpack = require('webpack');
// const PrerenderSpaPlugin = require('prerender-spa-plugin')
module.exports = {
    publicPath: './',
    outputDir: 'app/www',
    productionSourceMap: false,
    chainWebpack: config => {
        config
            .plugin('provide')
            .use(webpack.ProvidePlugin, [{
                $: 'jquery',
                jquery: 'jquery',
                jQuery: 'jquery',
                'window.jQuery': 'jquery'
            }]);

    },
    // configureWebpack: config => {
    //     if (process.env.NODE_ENV === 'production') {
    //         return {
    //             plugins: [
    //                 // new PrerenderSpaPlugin(
    //                 //     // Absolute path to compiled SPA
    //                 //     path.resolve(__dirname, 'dist'),
    //                 //     // List of routes to prerender
    //                 //     ['/', '/user/signUp', '/user/invest', '/user/Appraisal'],
    //                 //     {
    //                 //         // options
    //                 //     }
    //                 // ),
    //             ]
    //         }
    //     }
    // }
};
