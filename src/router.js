import Vue from 'vue'
import Router from 'vue-router'
import Test from './views/Test.vue'



Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
      //登入
      {
          // ---- cordova build使用
          // path: '*',
          // ---- apache, normal use
          path: '/',
          name: 'Test',
          component: Test
      },
  ]
})
