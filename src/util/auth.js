/* eslint-disable semi,no-trailing-spaces,indent,quotes,space-infix-ops,comma-dangle,padded-blocks,no-unused-vars,eol-last,semi-spacing */

let token = window.localStorage.getItem('token');
let admin_token = window.localStorage.getItem('admin_token');
let admin_level = window.localStorage.getItem('admin_level');
let admin_account = window.localStorage.getItem('admin_account');
let admin_name = window.localStorage.getItem('admin_name');

export default {
    // token: window.localStorage.getItem('token'),
    isLogin() {
        return token !== null && token !== '';
    },
    setToken(t) {
        window.localStorage.setItem('token', t + '');
        token = t;
    },
    getToken() {
        return token;
    },
    clearToken() {
        token = "";
        window.localStorage.removeItem('token');
    },
    // admin_token: window.localStorage.getItem('admin_token'),
    // admin_level: window.localStorage.getItem('admin_level'),
    // admin_account: window.localStorage.getItem('admin_account'),
    // admin_name: window.localStorage.getItem('admin_name'),
    isAdminLogin() {
        return admin_token !== null && admin_token !== '';
    },
    setAdminToken(t) {
        window.localStorage.setItem('admin_token', t + '');
        admin_token = t;
    },
    getAdminToken() {
        return admin_token;
    },
    clearAdminToken() {
        admin_token = '';
        window.localStorage.removeItem('admin_token');
    },
    // level
    setAdminLevel(l) {
        window.localStorage.setItem('admin_level', l + '');
        admin_level = l;
    },
    getAdminLevel() {
        return admin_level;
    },
    clearAdminLevel() {
        window.localStorage.removeItem('admin_level');
    },
    // account
    setAdminAccount(a) {
        window.localStorage.setItem('admin_account', a + '');
        admin_account = a;
    },
    getAdminAccount() {
        return admin_account;
    },
    setAdminName(n) {
        window.localStorage.setItem('admin_name', n + '');
        admin_name = n;
    },
    getAdminName() {
        return admin_name;
    },
    clearAdminAccount() {
        admin_account = '';
        window.localStorage.removeItem('admin_account');
    },
    getSocketURL(){
        return "wss://eternal-guardian.com/app-api/wss"
        // return "ws://192.168.0.236:8080/app-api/ws"
    },
    getKafKaSocketURL(){
        return "wss://eternal-guardian.com/app-api/wss/kafka"
        // return "ws://192.168.0.236:8080/app-api/ws/kafka"

    }

}
