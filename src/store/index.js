import Vue from 'vue'
import Vuex from 'vuex'
// import realName from 'src/store/realName'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        // state 类似 data
        //这里面写入数据

        /*實名認證*/
        verifiedType: '',
        name: '',
        country: '',
        birthday: '',
        verifiedNumber: '',
        address: '',
        billImage: '',
        passportImage: '',
        proveImage: '',

        /*判斷是否僅需上傳檔案*/
        onlyUpload: '',

        /*記憶轉帳資訊*/
        transferAmount: '',
        transferAddress: '',
        transferFromCoinName: '',
        transferNote: '',
    },
    getters: {
        // getters 类似 computed
        // 在这里面写个方法
    },
    mutations: {
        // mutations 类似methods
        // 写方法对数据做出更改(同步操作)

        /*實名認證*/
        setVerifiedType(state, value) {
            state.verifiedType = value
        },
        setName(state, value) {
            state.name = value
        },
        setCountry(state, value) {
            state.country = value
        },
        setBirthday(state, value) {
            state.birthday = value
        },
        setVerifiedNumber(state, value) {
            state.verifiedNumber = value
        },
        setAddress(state, value) {
            state.address = value
        },
        setBillImage(state, value) {
            state.billImage = value
        },
        setPassportImage(state, value) {
            state.passportImage = value
        },
        setProveImage(state, value) {
            state.proveImage = value
        },

        initRealName(state) {
            state.verifiedType = ''
            state.name = ''
            state.country = ''
            state.birthday = ''
            state.verifiedNumber = ''
            state.address = ''
            state.billImage = ''
            state.passportImage = ''
            state.proveImage = ''

        },
        /*圖片上傳*/
        setOnlyUpload(state, value) {
            state.onlyUpload = value
        },

        /*轉帳*/
        setTransferFromCoinName(state, value) {
            state.transferFromCoinName = value
        },
        setTransferAmount(state, value) {
            state.transferAmount = value
        },
        setTransferAddress(state, value) {
            state.transferAddress = value
        },
        setTransferNote(state, value) {
            state.transferNote = value
        },
        initTransfer(state) {
            state.transferFromCoinName = ''
            state.transferAmount = ''
            state.transferAddress = ''
            state.transferNote = ''
        }
    },
    actions: {
        // actions 类似methods
        // 写方法对数据做出更改(异步操作)

    },
    // 將整理好的 modules 放到 vuex 中組合
    modules: {},
    // 嚴格模式，禁止直接修改 state
    strict: true
});
