import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'


// ===================================== 外部套件 =====================================

//---- Bootstrap
import Bootstrap from 'bootstrap'
import BootstrapVue from 'bootstrap-vue'
Vue.use(BootstrapVue);

//---- Bootstrap
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

//---- MainCSS
import '../src/assets/theme/css/style_user.css'

//---- jQuery
import {$, jQuery} from 'jquery'

//---- LazyLoad
import VueLazyload from 'vue-lazyload'

//---- ElementUI
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(ElementUI);

//複製用
// import Clipboard from 'v-clipboard';
// Vue.use(Clipboard);
import VueClipboard from 'vue-clipboard2'
Vue.use(VueClipboard)


//---- LazyLoad
Vue.use(VueLazyload, {
    preLoad: 1.1,
    //error: 'dist/error.png',
    //loading: 'dist/loading.gif',
    attempt: 1,
    lazyComponent: true
});

Vue.config.productionTip = false;

// ===================================== Global 使用 =====================================

//---- Crypto
import Crypto from './util/crypto'
Crypto.install = function (Vue) {
    Vue.prototype.$crypto = Crypto;
};
Vue.use(Crypto);

//---- Auth
import Auth from './util/auth'
Auth.install = function (Vue) {
    Vue.prototype.$auth = Auth;
};
Vue.use(Auth);

import { Dialog } from 'vant';
Vue.use(Dialog);


import infiniteScroll from 'vue-infinite-scroll'
Vue.use(infiniteScroll);

//我的目標 倒數
import Countdown from './components/Countdown.vue'
Vue.component('Timer',Countdown)


function exitApp(){
    navigator.app.exitApp();
}

import Http from './util/http'

Http.install = function (Vue) {
    Vue.prototype.$http = Http;
};
Vue.use(Http);

//包app時要取消


    new Vue({

        Bootstrap,
        router,
        store,
        render: h => h(App)
    }).$mount('#app')


